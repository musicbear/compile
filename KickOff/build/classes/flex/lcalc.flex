
package src;
import java_cup.runtime.*;

%%
%class Lexer

%line
%column

%cup

%{

    private Symbol symbol(int type) {
        return new Symbol(type, yyline, yycolumn);
    }

    private Symbol symbol(int type, Object value) {
        return new Symbol(type, yyline, yycolumn, value);
    }

    private void debug(String type){
        if(type == "SEPARATOR")
            System.out.println("<"+type+">");
        else
            System.out.print("<"+type+">");
    } 

    private void debug(String type, String text){
        System.out.print("<"+type+","+text+">");
    } 
%}


LineTerminator = \r|\n|\r\n

WhiteSpace     = {LineTerminator} | [ \t\f]

int = 0 | [1-9][0-9]*

dot = "."
float = [0-9]+{dot}[0-9]+

digit    = [0-9]*
alphabet = [a-zA-Z ]*
specials = [!|@|#|$|%|\^|&|*|(|)]*
string   = \'{alphabet}{specials}{digit}\'

boolean = true|false

var = [A-Za-z_][A-Za-z_0-9]*


%%
<YYINITIAL> {

    "+"        { debug("PLUS");  return symbol(sym.PLUS); }
    "-"        { debug("MINUS"); return symbol(sym.MINUS); }
    "*"        { debug("TIMES"); return symbol(sym.TIMES); }
    "/"        { debug("DIVIDE"); return symbol(sym.DIVIDE); }
    ";"        { debug("SEMI"); return symbol(sym.SEMI); }
    ","        { debug("COMMA"); return symbol(sym.COMMA); }
    "="        { debug("EQUALS"); return symbol(sym.EQUALS); }

    "("        { debug("LPAREN"); return symbol(sym.LPAREN); }
    ")"        { debug("RPAREN"); return symbol(sym.RPAREN); }

    "=="       { debug("ET"); return symbol(sym.ET); }
    "!="       { debug("NET"); return symbol(sym.NET); }
    "<"        { debug("LT"); return symbol(sym.LT); }
    
    ">"        { debug("GT"); return symbol(sym.GT); }
   
 

    "int"      { debug("INT"); return symbol(sym.INT); }
    "float"    { debug("FLOAT"); return symbol(sym.FLOAT); }
    "boolean"  { debug("BOOL"); return symbol(sym.BOOL); }
    "string"   { debug("STRING"); return symbol(sym.STRING); }

    "if"       { debug("IF"); return symbol(sym.IF); }
    "endif"     { debug("ENDIF"); return symbol(sym.ENDIF); }
    "else"     { debug("ELSE"); return symbol(sym.ELSE); }
    "while"    { debug("WHILE"); return symbol(sym.WHILE); }

    

    "{"         { debug("BEGIN"); return symbol(sym.BEGIN); }
    "}"         { debug("END"); return symbol(sym.END); }   

    "print"    { debug("PRINT"); return symbol(sym.PRINT); }

    {int}      { debug("INT_LITERAL",yytext()); return symbol(sym.INT_LITERAL, new Integer(yytext())); }
    {float}    { debug("FLOAT_LITERAL",yytext()); return symbol(sym.FLOAT_LITERAL, new Float(yytext())); }
    {string}   { debug("STRING_LITERAL",yytext()); return symbol(sym.STRING_LITERAL, new String(yytext())); }
    {boolean}  { debug("BOOL_LITERAL",yytext()); return symbol(sym.BOL, new Boolean(yytext())); }

    {var}      { 
        Integer n = (Integer) SymTable.table.get(yytext());
        if (n == null) {
            SymTable.table.put(yytext(), 0);
        }
        debug("VAR", yytext() ); 
        return symbol(sym.VAR, yytext()); 
    }


    /* Don't do anything if whitespace is found */

    {WhiteSpace}       { /* just skip what was found, do nothing */ }
}

[^]                    { throw new Error("Illegal character <"+yytext()+">"); }