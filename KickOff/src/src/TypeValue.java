/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package src;


public class TypeValue {
    private Object value;
    private Type type;
    
    public TypeValue()
    {
        
    }

    public TypeValue(int value)
    {
        type = Type.integer();
        this.value = (Object) value;
    }
    
    public TypeValue(float value)
    {
        type = Type.floating_point();
        this.value = (Object) value;
    }
    
     public TypeValue(String value)
    {
        type = Type.string();
        this.value = (Object) value;
    }

    public TypeValue (boolean mutex)
    {
        type = Type.bool();
        this.value = (Object) mutex;
    }
    
    // Just for a placeholder (for overloading with two strings)
    public TypeValue(String func, String val)
    {
        this.value = (Object) null;
    }

    public Type getType()
    {
        return type;
    }

    public Object getValue()
    {
        return value;
    }

    public void setValue(Object value){
        this.value = value;
    }
}
