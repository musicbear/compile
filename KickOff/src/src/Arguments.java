/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package src;

import java.util.ArrayList;


public class Arguments {
    public ArrayList<Argument> argumentList = new ArrayList<Argument>();

    public Arguments(Argument a){
        argumentList  = new ArrayList<Argument>();
        argumentList.add(a);
    }

    public Arguments(Arguments aa, Argument a){
        argumentList = aa.argumentList;
        argumentList.add(a);
    }
}
