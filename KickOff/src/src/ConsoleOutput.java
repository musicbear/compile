/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package src;


public class ConsoleOutput {
    
    private static volatile ConsoleOutput instance = null;
    
    private ConsoleOutput() {};
    
    private String output = "";
    private OutputType outputType = OutputType.OUTPUT;
    
    public static ConsoleOutput getInstance(){
        if(instance == null){
            synchronized(ConsoleOutput.class){
                if(instance == null){
                    instance = new ConsoleOutput();
                }
            }
        }
        return instance;
    }
    
    public void setText(String text) {
        this.output = output + text + "\n";
    }
    
    public void setText(String text, OutputType type) {
        
        this.output = output + text + "\n";
        this.outputType = type;
    }
    
    public String getText() {
        return this.output;
    }
    
    public OutputType getOutputType() {
        return this.outputType;
    }
    
    public void clear() {
        this.output = "";
    }
    
    public enum OutputType {
        OUTPUT,
        ERROR
    }
}

