/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package src;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;


public class Environment {
    public static Hashtable<String,TypeValue> table = new Hashtable<>();
   
    public static void printGlobalTable(){
        System.out.println("Global Variables:");
        for(TypeValue value: table.values()) {
            System.out.println(value.getType().getCode() + ", " + value.getValue() + "\n");
        }
    }
    
    public static void printEnvTable() {
        System.out.println("\n***** ENV TABLE VALUES *****");

        System.out.println("Global Variables:");
        System.out.println("---------------------");
        for(String key: table.keySet()) {
            TypeValue value = table.get(key);
            System.out.println(key + ", "+ value.getType().getCode() + ", " + value.getValue());
        }
        System.out.println("---------------------");


        System.out.println("***** ENV TABLE VALUES *****");
    }
    
    public void a() {
        Environment.printEnvTable();
    }
}
